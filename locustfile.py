from locust import HttpLocust, TaskSet, task


class UserBehavior(TaskSet):

    @task(1)
    def check_registered_vehicle(self):
        headers = {
            'x-access-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjIsImV4cCI6MTUxMzc5MTIxMTYzNn0.QNcYd07iesaM-Zi4qHzJj3-sCJRMtdS7dqIPFeTdqpU'
        }
        self.client.get('/api/vehicles_is_registered?vehicle_UID=38&type=0&plate_letter1=B&plate_letter2=H&plate_letter3=J&plate_digit1=1&plate_digit2=3&plate_digit3=7&plate_digit4=', headers=headers, verify=False)


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 0
    max_wait = 1000
